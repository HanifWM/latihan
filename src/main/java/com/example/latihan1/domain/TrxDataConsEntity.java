package com.example.latihan1.domain;

import javax.persistence.*;
import java.sql.Time;

@Entity
@Table(name = "TRX_DATA_CONS", schema = "MP_REKON", catalog = "")
public class TrxDataConsEntity {
    private Long id;
    private Integer trxSts;
    private String trxDate;
    private String trxTime;
    private String terminalCode;
    private String cardNumber;
    private String mti;
    private String procCode;
    private String amount;
    private String respCode;
    private String acquirer;
    private String issuer;
    private String refNumber;
    private String bit11;
    private String bit32;
    private String bit38;
    private String bit43;
    private String bit49;
    private String rcvDate;
    private String rcvTime;
    private Time businessDate;
    private Byte channelTypeId;
    private String destination;
    private String destinationAccount;
    private String destinationOwner;
    private Byte transferIndicator;
    private String additionalRespCode;
    private String pointOfService;
    private String bit55;
    private String biller;

    @Basic
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "TRX_STS")
    public Integer getTrxSts() {
        return trxSts;
    }

    public void setTrxSts(Integer trxSts) {
        this.trxSts = trxSts;
    }

    @Basic
    @Column(name = "TRX_DATE")
    public String getTrxDate() {
        return trxDate;
    }

    public void setTrxDate(String trxDate) {
        this.trxDate = trxDate;
    }

    @Basic
    @Column(name = "TRX_TIME")
    public String getTrxTime() {
        return trxTime;
    }

    public void setTrxTime(String trxTime) {
        this.trxTime = trxTime;
    }

    @Basic
    @Column(name = "TERMINAL_CODE")
    public String getTerminalCode() {
        return terminalCode;
    }

    public void setTerminalCode(String terminalCode) {
        this.terminalCode = terminalCode;
    }

    @Basic
    @Column(name = "CARD_NUMBER")
    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    @Basic
    @Column(name = "MTI")
    public String getMti() {
        return mti;
    }

    public void setMti(String mti) {
        this.mti = mti;
    }

    @Basic
    @Column(name = "PROC_CODE")
    public String getProcCode() {
        return procCode;
    }

    public void setProcCode(String procCode) {
        this.procCode = procCode;
    }

    @Basic
    @Column(name = "AMOUNT")
    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    @Basic
    @Column(name = "RESP_CODE")
    public String getRespCode() {
        return respCode;
    }

    public void setRespCode(String respCode) {
        this.respCode = respCode;
    }

    @Basic
    @Column(name = "ACQUIRER")
    public String getAcquirer() {
        return acquirer;
    }

    public void setAcquirer(String acquirer) {
        this.acquirer = acquirer;
    }

    @Basic
    @Column(name = "ISSUER")
    public String getIssuer() {
        return issuer;
    }

    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }

    @Basic
    @Column(name = "REF_NUMBER")
    public String getRefNumber() {
        return refNumber;
    }

    public void setRefNumber(String refNumber) {
        this.refNumber = refNumber;
    }

    @Basic
    @Column(name = "BIT11")
    public String getBit11() {
        return bit11;
    }

    public void setBit11(String bit11) {
        this.bit11 = bit11;
    }

    @Basic
    @Column(name = "BIT32")
    public String getBit32() {
        return bit32;
    }

    public void setBit32(String bit32) {
        this.bit32 = bit32;
    }

    @Basic
    @Column(name = "BIT38")
    public String getBit38() {
        return bit38;
    }

    public void setBit38(String bit38) {
        this.bit38 = bit38;
    }

    @Basic
    @Column(name = "BIT43")
    public String getBit43() {
        return bit43;
    }

    public void setBit43(String bit43) {
        this.bit43 = bit43;
    }

    @Basic
    @Column(name = "BIT49")
    public String getBit49() {
        return bit49;
    }

    public void setBit49(String bit49) {
        this.bit49 = bit49;
    }

    @Basic
    @Column(name = "RCV_DATE")
    public String getRcvDate() {
        return rcvDate;
    }

    public void setRcvDate(String rcvDate) {
        this.rcvDate = rcvDate;
    }

    @Basic
    @Column(name = "RCV_TIME")
    public String getRcvTime() {
        return rcvTime;
    }

    public void setRcvTime(String rcvTime) {
        this.rcvTime = rcvTime;
    }

    @Basic
    @Column(name = "BUSINESS_DATE")
    public Time getBusinessDate() {
        return businessDate;
    }

    public void setBusinessDate(Time businessDate) {
        this.businessDate = businessDate;
    }

    @Basic
    @Column(name = "CHANNEL_TYPE_ID")
    public Byte getChannelTypeId() {
        return channelTypeId;
    }

    public void setChannelTypeId(Byte channelTypeId) {
        this.channelTypeId = channelTypeId;
    }

    @Basic
    @Column(name = "DESTINATION")
    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    @Basic
    @Column(name = "DESTINATION_ACCOUNT")
    public String getDestinationAccount() {
        return destinationAccount;
    }

    public void setDestinationAccount(String destinationAccount) {
        this.destinationAccount = destinationAccount;
    }

    @Basic
    @Column(name = "DESTINATION_OWNER")
    public String getDestinationOwner() {
        return destinationOwner;
    }

    public void setDestinationOwner(String destinationOwner) {
        this.destinationOwner = destinationOwner;
    }

    @Basic
    @Column(name = "TRANSFER_INDICATOR")
    public Byte getTransferIndicator() {
        return transferIndicator;
    }

    public void setTransferIndicator(Byte transferIndicator) {
        this.transferIndicator = transferIndicator;
    }

    @Basic
    @Column(name = "ADDITIONAL_RESP_CODE")
    public String getAdditionalRespCode() {
        return additionalRespCode;
    }

    public void setAdditionalRespCode(String additionalRespCode) {
        this.additionalRespCode = additionalRespCode;
    }

    @Basic
    @Column(name = "POINT_OF_SERVICE")
    public String getPointOfService() {
        return pointOfService;
    }

    public void setPointOfService(String pointOfService) {
        this.pointOfService = pointOfService;
    }

    @Basic
    @Column(name = "BIT55")
    public String getBit55() {
        return bit55;
    }

    public void setBit55(String bit55) {
        this.bit55 = bit55;
    }

    @Basic
    @Column(name = "BILLER")
    public String getBiller() {
        return biller;
    }

    public void setBiller(String biller) {
        this.biller = biller;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TrxDataConsEntity that = (TrxDataConsEntity) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (trxSts != null ? !trxSts.equals(that.trxSts) : that.trxSts != null) return false;
        if (trxDate != null ? !trxDate.equals(that.trxDate) : that.trxDate != null) return false;
        if (trxTime != null ? !trxTime.equals(that.trxTime) : that.trxTime != null) return false;
        if (terminalCode != null ? !terminalCode.equals(that.terminalCode) : that.terminalCode != null) return false;
        if (cardNumber != null ? !cardNumber.equals(that.cardNumber) : that.cardNumber != null) return false;
        if (mti != null ? !mti.equals(that.mti) : that.mti != null) return false;
        if (procCode != null ? !procCode.equals(that.procCode) : that.procCode != null) return false;
        if (amount != null ? !amount.equals(that.amount) : that.amount != null) return false;
        if (respCode != null ? !respCode.equals(that.respCode) : that.respCode != null) return false;
        if (acquirer != null ? !acquirer.equals(that.acquirer) : that.acquirer != null) return false;
        if (issuer != null ? !issuer.equals(that.issuer) : that.issuer != null) return false;
        if (refNumber != null ? !refNumber.equals(that.refNumber) : that.refNumber != null) return false;
        if (bit11 != null ? !bit11.equals(that.bit11) : that.bit11 != null) return false;
        if (bit32 != null ? !bit32.equals(that.bit32) : that.bit32 != null) return false;
        if (bit38 != null ? !bit38.equals(that.bit38) : that.bit38 != null) return false;
        if (bit43 != null ? !bit43.equals(that.bit43) : that.bit43 != null) return false;
        if (bit49 != null ? !bit49.equals(that.bit49) : that.bit49 != null) return false;
        if (rcvDate != null ? !rcvDate.equals(that.rcvDate) : that.rcvDate != null) return false;
        if (rcvTime != null ? !rcvTime.equals(that.rcvTime) : that.rcvTime != null) return false;
        if (businessDate != null ? !businessDate.equals(that.businessDate) : that.businessDate != null) return false;
        if (channelTypeId != null ? !channelTypeId.equals(that.channelTypeId) : that.channelTypeId != null)
            return false;
        if (destination != null ? !destination.equals(that.destination) : that.destination != null) return false;
        if (destinationAccount != null ? !destinationAccount.equals(that.destinationAccount) : that.destinationAccount != null)
            return false;
        if (destinationOwner != null ? !destinationOwner.equals(that.destinationOwner) : that.destinationOwner != null)
            return false;
        if (transferIndicator != null ? !transferIndicator.equals(that.transferIndicator) : that.transferIndicator != null)
            return false;
        if (additionalRespCode != null ? !additionalRespCode.equals(that.additionalRespCode) : that.additionalRespCode != null)
            return false;
        if (pointOfService != null ? !pointOfService.equals(that.pointOfService) : that.pointOfService != null)
            return false;
        if (bit55 != null ? !bit55.equals(that.bit55) : that.bit55 != null) return false;
        if (biller != null ? !biller.equals(that.biller) : that.biller != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (trxSts != null ? trxSts.hashCode() : 0);
        result = 31 * result + (trxDate != null ? trxDate.hashCode() : 0);
        result = 31 * result + (trxTime != null ? trxTime.hashCode() : 0);
        result = 31 * result + (terminalCode != null ? terminalCode.hashCode() : 0);
        result = 31 * result + (cardNumber != null ? cardNumber.hashCode() : 0);
        result = 31 * result + (mti != null ? mti.hashCode() : 0);
        result = 31 * result + (procCode != null ? procCode.hashCode() : 0);
        result = 31 * result + (amount != null ? amount.hashCode() : 0);
        result = 31 * result + (respCode != null ? respCode.hashCode() : 0);
        result = 31 * result + (acquirer != null ? acquirer.hashCode() : 0);
        result = 31 * result + (issuer != null ? issuer.hashCode() : 0);
        result = 31 * result + (refNumber != null ? refNumber.hashCode() : 0);
        result = 31 * result + (bit11 != null ? bit11.hashCode() : 0);
        result = 31 * result + (bit32 != null ? bit32.hashCode() : 0);
        result = 31 * result + (bit38 != null ? bit38.hashCode() : 0);
        result = 31 * result + (bit43 != null ? bit43.hashCode() : 0);
        result = 31 * result + (bit49 != null ? bit49.hashCode() : 0);
        result = 31 * result + (rcvDate != null ? rcvDate.hashCode() : 0);
        result = 31 * result + (rcvTime != null ? rcvTime.hashCode() : 0);
        result = 31 * result + (businessDate != null ? businessDate.hashCode() : 0);
        result = 31 * result + (channelTypeId != null ? channelTypeId.hashCode() : 0);
        result = 31 * result + (destination != null ? destination.hashCode() : 0);
        result = 31 * result + (destinationAccount != null ? destinationAccount.hashCode() : 0);
        result = 31 * result + (destinationOwner != null ? destinationOwner.hashCode() : 0);
        result = 31 * result + (transferIndicator != null ? transferIndicator.hashCode() : 0);
        result = 31 * result + (additionalRespCode != null ? additionalRespCode.hashCode() : 0);
        result = 31 * result + (pointOfService != null ? pointOfService.hashCode() : 0);
        result = 31 * result + (bit55 != null ? bit55.hashCode() : 0);
        result = 31 * result + (biller != null ? biller.hashCode() : 0);
        return result;
    }
}
