package com.example.latihan1.Model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "TRX_DATA_CONS")
public class transaksiModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="ID")
    private Integer id;

    @Column(name="TRX_STS")
    private Integer TRX_STS;

    @Column(name="TRX_DATE")
    private String trxDate;

    @Column(name="TRX_TIME")
    private String trxTime;

    @Column(name="TERMINAL_CODE")
    private String terminalCode;

    @Column(name="CARD_NUMBER")
    private String cardNumber;

    @Column(name="MTI")
    private String mti;

    @Column(name="PROC_CODE")
    private String procCode;

    @Column(name="AMOUNT")
    private String amount;

    @Column(name="RESP_CODE")
    private String respCode;

    @Column(name="ACQUIRER")
    private String acquirer;

    @Column(name="ISSUER")
    private String issuer;

    @Column(name="REF_NUMBER")
    private String refNumber;

    @Column(name="BIT11")
    private String bit11;

    @Column(name="BIT32")
    private String bit32;

    @Column(name="BIT38")
    private String bit38;

    @Column(name="BIT43")
    private String bit43;

    @Column(name="BIT49")
    private String bit49;

    @Column(name="RCV_DATE")
    private String rcvDate;

    @Column(name="RCV_TIME")
    private String rcvTime;

    @Column(name="BUSINESS_DATE")
    private Date businessDate;

    @Column(name="CHANNEL_TYPE_ID")
    private Integer channelTypeId;

    @Column(name="DESTINATION")
    private String destination;

    @Column(name="DESTINATION_ACCOUNT")
    private String destinationAccount;

    @Column(name="DESTINATION_OWNER")
    private String destinationOwner;

    @Column(name="TRANSFER_INDICATOR")
    private Integer transferIndicator;

    @Column(name="ADDITIONAL_RESP_COE")
    private String additionalRespCode;

    @Column(name="POINT_OF_SERVICE")
    private String pointOfService;

    @Column(name="BIT55")
    private String bit55;

    @Column(name="BILLER")
    private String biller;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTRX_STS() {
        return TRX_STS;
    }

    public void setTRX_STS(Integer TRX_STS) {
        this.TRX_STS = TRX_STS;
    }

    public String getTrxDate() {
        return trxDate;
    }

    public void setTrxDate(String trxDate) {
        this.trxDate = trxDate;
    }

    public String getTrxTime() {
        return trxTime;
    }

    public void setTrxTime(String trxTime) {
        this.trxTime = trxTime;
    }

    public String getTerminalCode() {
        return terminalCode;
    }

    public void setTerminalCode(String terminalCode) {
        this.terminalCode = terminalCode;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getMti() {
        return mti;
    }

    public void setMti(String mti) {
        this.mti = mti;
    }

    public String getProcCode() {
        return procCode;
    }

    public void setProcCode(String procCode) {
        this.procCode = procCode;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getRespCode() {
        return respCode;
    }

    public void setRespCode(String respCode) {
        this.respCode = respCode;
    }

    public String getAcquirer() {
        return acquirer;
    }

    public void setAcquirer(String acquirer) {
        this.acquirer = acquirer;
    }

    public String getIssuer() {
        return issuer;
    }

    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }

    public String getRefNumber() {
        return refNumber;
    }

    public void setRefNumber(String refNumber) {
        this.refNumber = refNumber;
    }

    public String getBit11() {
        return bit11;
    }

    public void setBit11(String bit11) {
        this.bit11 = bit11;
    }

    public String getBit32() {
        return bit32;
    }

    public void setBit32(String bit32) {
        this.bit32 = bit32;
    }

    public String getBit38() {
        return bit38;
    }

    public void setBit38(String bit38) {
        this.bit38 = bit38;
    }

    public String getBit43() {
        return bit43;
    }

    public void setBit43(String bit43) {
        this.bit43 = bit43;
    }

    public String getBit49() {
        return bit49;
    }

    public void setBit49(String bit49) {
        this.bit49 = bit49;
    }

    public String getRcvDate() {
        return rcvDate;
    }

    public void setRcvDate(String rcvDate) {
        this.rcvDate = rcvDate;
    }

    public String getRcvTime() {
        return rcvTime;
    }

    public void setRcvTime(String rcvTime) {
        this.rcvTime = rcvTime;
    }

    public Date getBusinessDate() {
        return businessDate;
    }

    public void setBusinessDate(Date businessDate) {
        this.businessDate = businessDate;
    }

    public Integer getChannelTypeId() {
        return channelTypeId;
    }

    public void setChannelTypeId(Integer channelTypeId) {
        this.channelTypeId = channelTypeId;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getDestinationAccount() {
        return destinationAccount;
    }

    public void setDestinationAccount(String destinationAccount) {
        this.destinationAccount = destinationAccount;
    }

    public String getDestinationOwner() {
        return destinationOwner;
    }

    public void setDestinationOwner(String destinationOwner) {
        this.destinationOwner = destinationOwner;
    }

    public Integer getTransferIndicator() {
        return transferIndicator;
    }

    public void setTransferIndicator(Integer transferIndicator) {
        this.transferIndicator = transferIndicator;
    }

    public String getAdditionalRespCode() {
        return additionalRespCode;
    }

    public void setAdditionalRespCode(String additionalRespCode) {
        this.additionalRespCode = additionalRespCode;
    }

    public String getPointOfService() {
        return pointOfService;
    }

    public void setPointOfService(String pointOfService) {
        this.pointOfService = pointOfService;
    }

    public String getBit55() {
        return bit55;
    }

    public void setBit55(String bit55) {
        this.bit55 = bit55;
    }

    public String getBiller() {
        return biller;
    }

    public void setBiller(String biller) {
        this.biller = biller;
    }
}
