package com.example.latihan1.ServiceImpl;

import com.example.latihan1.Dao.transaksiDao;
import com.example.latihan1.Model.transaksiModel;
import com.example.latihan1.Service.transaksiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class transaksiServiceImpl implements transaksiService {
    @Autowired
    transaksiDao trxDao;

    @Override
    public List<transaksiModel> getTransaksi(){
        return trxDao.getTransaksi();
    }

    @Override
    public  transaksiModel findTransaksi(int id){
        return trxDao.findTransaksi(id);
    }

    @Override
    public  transaksiModel saveTransaksi(transaksiModel trxModel){
        return trxDao.saveTransaksi(trxModel);
    }

    @Override
    public int deleteTransaksi(int id){
        trxDao.deleteTransaksi(id);
        return id;
    }
}
