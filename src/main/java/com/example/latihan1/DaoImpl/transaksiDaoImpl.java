package com.example.latihan1.DaoImpl;

import com.example.latihan1.Dao.transaksiDao;
import com.example.latihan1.Model.transaksiModel;
import com.example.latihan1.domain.TrxDataConsEntity;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.annotations.common.util.impl.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.cfg.Configuration;

import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

@Repository
public class transaksiDaoImpl implements transaksiDao {
    private static EntityManager entityManager;

    @Autowired
    public transaksiDaoImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public List<transaksiModel> getTransaksi(){
        //Query theQuery = (Query) entityManager.createQuery("SELECT e.trxDate, e.trxTime, e.cardNumber, e.refNumber, e.terminalCode, e.procCode FROM transaksiModel e WHERE e.businessDate = to_date('29-06-2020','dd-MM-yyyy')");
        Query theQuery = (Query) entityManager.createQuery("FROM transaksiModel e WHERE e.trxDate = '200101'");
        List<transaksiModel>transaksi = theQuery.getResultList();

        return transaksi;
    }

    @Override
    public transaksiModel findTransaksi(int id){
        transaksiModel trx = entityManager.find(transaksiModel.class, id);
        return trx;
    }
    @Override
    public transaksiModel saveTransaksi(transaksiModel trxModel){
        transaksiModel dbTransaksi = entityManager.merge(trxModel);
        trxModel.setId(dbTransaksi.getId());
        return trxModel;
    }

    @Override
    public void deleteTransaksi(int id){
        Query theQuery = (Query) entityManager.createQuery("delete from transaksiModel where id =:transaksiId");
        theQuery.setParameter("transaksiId", id);
        theQuery.executeUpdate();
    }

    public static void main(String[] args){

        Logger logger = Logger.getLogger("MyLog");
        FileHandler fh;
        try{
            //Deklarasi Var
            Date dateSekarang = new Date();
            SimpleDateFormat formatTglSekarang = new SimpleDateFormat("yyMMdd");
            String tglSekarang = formatTglSekarang.format(dateSekarang);
            String tgl = new String();
            Integer jumlah = 0;
            if(args.length != 0){
                for(int i =0;i<args.length;i++){
                    tgl = args[i];
                    tgl = tgl.replaceAll("-","");
                    tgl = tgl.substring(2);
                    System.out.println(tgl);
                }
            }
            //dapetin tgl sekarang jika args null
            else{

                Date date = new Date();
                SimpleDateFormat formatTgl = new SimpleDateFormat("yyMMdd");
                tgl = formatTgl.format(date);
                System.out.println(tgl);
            }
            File file = new File("DATA_"+tgl+".txt");
            if(!file.exists()){
                file.createNewFile();
            }

            EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("org.hibernate.tutorial.jpa");
            EntityManager entityManager = entityManagerFactory.createEntityManager();
            //Query query = entityManager.createQuery("From transaksiModel e where e.trxDate ="+tgl);
            Query query = entityManager.createQuery("From transaksiModel e where e.trxDate = "+tgl+" AND rownum <= 10");
            List<transaksiModel>list1 = query.getResultList();

            PrintWriter pw = new PrintWriter(file);
            for(transaksiModel e:list1) {
                System.out.println("Transaksi ID = "+ e.getId());
                System.out.println("Terminal Code = "+e.getTerminalCode());
            }
            for(transaksiModel e:list1) {
                pw.println("Transaksi ID = "+ e.getId());
                pw.println("Transaksi Date = "+e.getTrxDate());
                pw.println("Transaksi Time = "+e.getTrxTime());
                pw.println("Transaksi Card Number = "+e.getCardNumber());
                pw.println("Transaksi Ref Number"+ e.getRefNumber());
                pw.println("Transaksi Terminal Code = "+e.getTerminalCode());
                pw.println("Transaksi ProcCode = "+e.getProcCode());
                jumlah++;
            }
            pw.close();
            System.out.println(tgl);

            //konfigurasi log
            fh = new FileHandler("C:\\Users\\Hanif\\IdeaProjects\\latihan1\\target\\log\\Log_"+tglSekarang+".log");
            logger.addHandler(fh);
            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);

            //Melakukan Logging
            String strJumlah = Integer.toString(jumlah);
            logger.severe("==================Aplikasi Running=====================");

            logger.info(strJumlah);
            logger.severe("==================Aplikasi Finish======================");

            System.out.println("Done");
        } catch (IOException e){
            e.printStackTrace();
        }
    }
}
