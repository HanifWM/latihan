package com.example.latihan1.Dao;

import com.example.latihan1.Model.transaksiModel;

import java.util.List;

public interface transaksiDao {
    public List<transaksiModel> getTransaksi();
    public transaksiModel findTransaksi(int id);
    public transaksiModel saveTransaksi(transaksiModel trxModel);
    void deleteTransaksi(int id);
}
