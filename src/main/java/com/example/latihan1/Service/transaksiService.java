package com.example.latihan1.Service;

import com.example.latihan1.Model.transaksiModel;

import java.util.List;

public interface transaksiService {
    public List<transaksiModel> getTransaksi();
    public  transaksiModel findTransaksi(int id);
    public  transaksiModel saveTransaksi(transaksiModel trxModel);
    int deleteTransaksi(int id);
}
