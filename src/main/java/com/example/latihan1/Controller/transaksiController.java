package com.example.latihan1.Controller;

import com.example.latihan1.Model.transaksiModel;
import com.example.latihan1.Service.transaksiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class transaksiController {

    @Autowired
    private transaksiService trxService;

    @GetMapping("/DATA_29062020")
    public List<transaksiModel> getTransaksi(){
        return trxService.getTransaksi();
    }

    @PostMapping("/DATA_29062020")
    public transaksiModel addTransaksi(@RequestBody transaksiModel trxModel){
        return trxService.saveTransaksi(trxModel);
    }

    @DeleteMapping("/DATA_29062020/{id}")
    public String deleteTransaksi(@PathVariable int id){
        transaksiModel tempTrx = trxService.findTransaksi(id);
        if(tempTrx == null){
            throw new RuntimeException("Transaksi Tidak Di Temukan");
        }
        trxService.deleteTransaksi(id);
        return  "Delete Berhasil";
    }
}
